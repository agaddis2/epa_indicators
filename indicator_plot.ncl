;;;
; This script contains procedures to plot EPA and climate model indicators of climate  
; change. Procedures are meant to be called as needed after desired analysis has been
; performed in a parent script. Default plot settings may be manipulated to change
; the overall look of plots produced using this script.
;
; Abigail L. Gaddis, ORNL- CESG
; Oct 23, 2013
;;;

; Defining basic plot settings for all functions

; General settings for xy plots

res_xy = True
res_xy@tiMainFontHeightF        = 0.018
res_xy@tiXAxisFontHeightF       = 0.018
res_xy@tiYAxisFontHeightF       = 0.018
res_xy@tmLabelAutoStride        = True 
res_xy@tmXTOn                   = False
res_xy@tmYROn                   = False

; General settings for contour plots

res_contour = True
res_contour@cnFillOn                 = True
res_contour@gsnSpreadColors          = True
res_contour@gsnAddCyclic             = True
res_contour@cnLinesOn                = False        ; True is default
res_contour@cnLineLabelsOn           = False        ; True is default
res_contour@cnInfoLabelOn            = False
;res_contour@tmLabelAutoStride        = True
res_contour@tmYROn                   = False
res_contour@tmXTOn                   = False

;res_contour@lbLabelAutoStride        = True
;res_contour@lbLabelFontHeightF       = 0.010
;res_contour@gsnScale  = True        ; force text scaling
;res_contour@lbLabelBarOn            = True
;res_contour@cnLevelSelectionMode    =  "ManualLevels"
;res_contour@cnMinLevelValF          = 0


coloropt = True
coloropt@NumColorsInTable = 20
colors = (/"white","cyan","navyblue", "black"/)
bluefade = span_named_colors(colors,coloropt)

; Plots formatted dates vs time series data

procedure plot_time_series (data, time)
local wks, date, plot
begin
    wks = gsn_open_wks("pdf", "timeseries")
    res_time_series = res_xy
    date = cd_calendar(time, 1)
    date = yyyymm_to_yyyyfrac(date,0)
    res_time_series@tmXBFormat = "f"
    res_time_series@tiMainString = data@long_name
    res_time_series@tiXAxisString = "Time (years)"
    res_time_series@tiYAxisString = "Anomaly ("+data@units+")"
    res_time_series@gsnYRefLine              = 0.0
    plot = gsn_csm_xy(wks,date,data,res_time_series)
end

procedure plot_globe_map (data)
local wks, plot
begin
    wks = gsn_open_wks("pdf", "globalmap")
;    gsn_define_colormap(wks,bluefade)
    gsn_define_colormap(wks,"BlueDarkRed18")
;    gsn_define_colormap(wks,"temp_diff_18lev")
;    gsn_define_colormap(wks,"hotcold_18lev")
    res_map = res_contour
    plot = gsn_csm_contour_map(wks,data,res_map)
end

procedure plot_ensemble_ts (ensembleav, std, time)
local wks, date, plot
begin
    nmon = dimsizes(time)
    date = cd_calendar(time, 1)
    date = yyyymm_to_yyyyfrac(date,0)

    wks = gsn_open_wks("pdf", "ensemble_ts")
    res_ensemble                    = res_xy
    res_ensemble@gsnDraw            = False             ; don't draw yet
    res_ensemble@gsnFrame           = False             ; don't advance frame yet
    res_ensemble@tmXBFormat         = "f"
    res_ensemble@xyLineColors       = (/"blue"/)
    res_ensemble@tiMainString       = ensembleav@long_name
    res_ensemble@tiXAxisString      = "Time (years)"
    res_ensemble@tiYAxisString      = "Anomaly ("+ensembleav@units+")"
    res_ensemble@gsnYRefLine        = 0.0
    gsres                           = True
    gsres@tfPolyDrawOrder           = "Predraw"  
    gsres@gsFillColor               = "LightBlue"

    ; Calculating the maximum value for Y axis

    topline = ensembleav + std
    botline = ensembleav - std
    maxplot = max(topline)            
    minplot = min(botline)
    res_ensemble@trYMaxF                  = maxplot
    res_ensemble@trYMinF                  = minplot

    xp = new((/2*nmon/), typeof(time))
    yp = new((/2*nmon/), float)
    do t=0,nmon-1 
       yp(t)          = topline(t)
       yp(2*nmon-1-t) = botline(t)
       xp(t)          = date(t)
       xp(2*nmon-1-t) = date(t)
    end do

    plot = gsn_csm_xy(wks,date,ensembleav,res_ensemble)
    poly = gsn_add_polygon(wks,plot,xp,yp,gsres)

    draw(plot)
    frame(wks)
end

